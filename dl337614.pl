% Dawid Lazarczyk
:- use_module(library(lists)).
% I assume that grammar passed to arguments is a correct grammar, that is:
% * all nonterminals can be reached from the start symbol
% * there are no empty productions list (there are no prod(_, []))
% Abbreviations:
% Nt == Nonterminal


% analizaLL(+Grammar, +Word, -Tree)
analizaLL([prod(Start, ProdL) | Gr], Word, Tree) :-
    checkWord([prod(Start, ProdL) | Gr], Word, Start, [], Tree).


% checkWord(+Grammar, +Word, +Nt, -RestOfWord, -Tree)
checkWord(Gr, Word, Nt, RestOfWord, Tree) :-
    member(prod(Nt, ProdL), Gr),
    member(Prod, ProdL),
    prodMatch(Prod, Gr, Word, RestOfWord, TreeL),
    Tree=..[Nt | TreeL].


% prodMatch(+Production, +Grammar, +Word, -RestOfWord, -TreeList)
prodMatch([], _, Word, Word, []).

prodMatch([nt(Nt) | Prod], Gr, Word, RestOfWord, [Tree | TreeL]) :-
    checkWord(Gr, Word, Nt, RestOfWord1, Tree),
    prodMatch(Prod, Gr, RestOfWord1, RestOfWord, TreeL).

prodMatch([X | Prod], Gr, [X | Word], RestOfWord, [X | TreeL]) :-
    terminal(X),
    prodMatch(Prod, Gr, Word, RestOfWord, TreeL).


% jestCykl(+Grammar)
jestCykl(Gr) :- \+ noCycle(Gr, nullable).

% isLhsRecursion(+Grammar)
isLhsRecursion(Gr) :- \+ noCycle(Gr, nonnullable).


% noCycle(+Grammar, +Nullable)
% Depending on value of nullable we have different definitions of cycle.
% If Nullable = nullable, we demand that A -> A, if Nullable = nonnullable
% we want only A -> AB
noCycle(Gr, Null) :-
    Gr = [prod(St, _) | _],
    eps(Gr, EpsL),
    noCycle(St, grInfo(Gr, EpsL), [], [St], Null, _).


% noCycle(+Nt, +GrammarInfo, +VisitedNtsIn, +NtsOnStack, +Nullable,
%         -VisitedNtsOut)
% We're going through the graph using DFS, keeping list of vertices on the
% stack to find a potential cycle and remembering the vertices we've already
% visited.
noCycle(Nt, GrInfo, VisI, Stack, Null, [Nt | VisO]) :-
    GrInfo = grInfo(Gr, _),
    member(prod(Nt, ProdL), Gr),
    noCycleProds(ProdL, GrInfo, VisI, Stack, Null, VisO).


% noCycleProds(+ProductionsList, +GrammarInfo, +VisitedNtsIn, +NtsOnStack,
%              +Nullable, -VisitedNtsOut)
noCycleProds([], _, Vis, _, _, Vis).

noCycleProds([Prod | ProdL], GrInfo, VisI, Stack, Null, VisO) :-
    noCycleProd(Prod, GrInfo, VisI, Stack, Null, VisO1),
    noCycleProds(ProdL, GrInfo, VisO1, Stack, Null, VisO).


% noCycleProd(+Production, +GrammarInfo, +VisitedNtsIn, +NtsOnStack,
%             +Nullable, -VisitedNtsOut)
% We are looking for potential edges in the graph - that is nonterminals which
% can be derived from current production.
% Invariant: the part of the production we've already seen is nullable (the
% empty word can be derived from it).
noCycleProd([], _, Vis, _, _, Vis).

noCycleProd([nt(Nt) | Prod], GrInfo, VisI, Stack, Null, VisO) :-
    GrInfo = grInfo(_, EpsL),
    (isTailEps(Prod, EpsL, Null), nonmember(Nt, VisI) ->
        nonmember(Nt, Stack), % otherwise we have found a cycle
        noCycle(Nt, GrInfo, VisI, [Nt | Stack], Null, VisO1)
    ;
        VisO1 = VisI
    ),
    (member(Nt, EpsL) ->
        noCycleProd(Prod, GrInfo, VisO1, Stack, Null, VisO)
    ;
        VisO = VisO1
    ).

noCycleProd([X | _], _, Vis, _, _, Vis) :-
    terminal(X).

% isTailEps(+Production, +EpsilonNts, +NullableTail)
% Checking whether production is empty or NullableTail is equal to 1
isTailEps(_, _, nonnullable).

isTailEps(Prod, EpsL, nullable) :- isProdEps(Prod, EpsL).


% eps(+Grammar, -EpsilonNts)
% Looking for the nonterminals, from which we can derive an empty word.
eps(Gr, EpsL) :-
    epsIter(Gr, [], EpsL).


% epsIter(+Grammar, -EpsilonNtsIn, +EpsilonNtsOut)
% Enlarging the list of nullable nonterminals as long as possible.
epsIter(Gr, EpsLI, EpsLO) :-
    epsNts(Gr, EpsLI, EpsLO1),
    length(EpsLI, SizeI),
    length(EpsLO1, SizeO),
    (SizeO > SizeI ->
        epsIter(Gr, EpsLO1, EpsLO)
    ;
        EpsLO = EpsLO1
    ).


% epsNts(+Grammar, -EpsilonNtsIn, +EpsilonNtsOut)
epsNts([], EpsL, EpsL).

epsNts([prod(Nt, ProdL) | Gr], EpsLI, EpsLO) :-
    (nonmember(Nt, EpsLI), member(Prod, ProdL), isProdEps(Prod, EpsLI) ->
        EpsLI1 = [Nt | EpsLI]
    ;
        EpsLI1 = EpsLI
    ),
    epsNts(Gr, EpsLI1, EpsLO).


% isProdEps(+Production, +EpsilonNts)
% Checking whether the productions is nullable.
isProdEps([], _).

isProdEps([nt(Nt) | Prod], EpsL) :-
    member(Nt, EpsL),
    isProdEps(Prod, EpsL).


% first(+Grammar, -First)
% Finding the FIRST' sets for all nonterminals.
% The single set will be first(Nt, FirstSet).
first(Gr, FirstL) :-
    eps(Gr, EpsL),
    initMap(Gr, first, FirstLI),
    firstIter(Gr, grInfo(Gr, EpsL), FirstLI, FirstL).


% firstIter(+Grammar, +GrammarInfo, +FirstIn, -FirstOut)
% Enlarging the first sets as long as possible.
firstIter(Gr, GrInfo, FirstI, FirstO) :-
    firstNts(Gr, GrInfo, FirstI, FirstO1),
    countAllVals(FirstI, first, SizeI),
    countAllVals(FirstO1, first, SizeO),
    (SizeO > SizeI ->
        firstIter(Gr, GrInfo, FirstO1, FirstO)
    ;
        FirstO = FirstO1
    ).


% firstNts(+Grammar, +GrammarInfo, +FirstIn, -FirstOut)
firstNts([], _, FirstL, FirstL).

firstNts([prod(Nt, ProdL) | Gr], GrInfo, FirstI, FirstO) :-
    firstProds(ProdL, GrInfo, Nt, FirstI, FirstO1),
    firstNts(Gr, GrInfo, FirstO1, FirstO).


% firstProds(+ProductionsList, +GrammarInfo, +Nt, +FirstIn, -FirstOut)
firstProds([], _, _, First, First).

firstProds([Prod | ProdL], GrInfo, Nt, FirstI, FirstO) :-
    firstProd(Prod, GrInfo, Nt, FirstI, FirstO1),
    firstProds(ProdL, GrInfo, Nt, FirstO1, FirstO).


% firstProd(+Production, +GrInfo, +Nt, +FirstIn, -FirstOut)
firstProd([], _, _, First, First).

firstProd([nt(Nt1) | Prod], GrInfo, Nt2, FirstI, FirstO) :-
    member(first(Nt1, Nt1Val), FirstI),
    member(first(Nt2, Nt2Val), FirstI),
    addListsRemDups(Nt1Val, Nt2Val, Nt2Val1),
    changeMapVal(first(Nt2, Nt2Val1), first, FirstI, FirstO1),
    GrInfo = grInfo(_, EpsL),
    (member(Nt1, EpsL) ->
        firstProd(Prod, GrInfo, Nt2, FirstO1, FirstO)
    ;
        FirstO = FirstO1
    ).

firstProd([X | _], _, Nt, FirstI, FirstO) :-
    terminal(X),
    member(first(Nt, NtVal), FirstI),
    (nonmember(X, NtVal) ->
        changeMapVal(first(Nt, [X | NtVal]), first, FirstI, FirstO)
    ;
        FirstO = FirstI
    ).


% follow(+Grammar, -Follow)
% Finding the follow sets. The single set will be follow(Nt, NtFollow).
follow(Gr, Follow) :-
    eps(Gr, EpsL),
    first(Gr, First),
    initMap(Gr, follow, FollowI),
    Gr = [prod(St, _) | _],
    changeMapVal(follow(St, [#]), follow, FollowI, FollowI1),
    followIter(Gr, grInfo(Gr, EpsL, First), FollowI1, Follow).


% followIter(+Grammar, +GrammarInfo, +FollowIn, -FollowOut)
% Enlarging the follow sets as long as possible.
followIter(Gr, GrInfo, FollowI, FollowO) :-
    followNts(Gr, GrInfo, FollowI, FollowO1),
    countAllVals(FollowI, follow, SizeI),
    countAllVals(FollowO1, follow, SizeO),
    (SizeO > SizeI ->
        followIter(Gr, GrInfo, FollowO1, FollowO)
    ;
        FollowO = FollowO1
    ).


% followNts(+Grammar, +GrammarInfo, +FollowIn, -FollowOut)
followNts([], _, Follow, Follow).

followNts([prod(Nt, ProdL) | Gr], GrInfo, FollowI, FollowO) :-
    followProds(ProdL, GrInfo, Nt, FollowI, FollowO1),
    followNts(Gr, GrInfo, FollowO1, FollowO).


% followProds(+ProductionsList, +GrammarInfo, +Nt, +FollowIn, -FollowOut)
followProds([], _, _, Follow, Follow).

followProds([Prod | ProdL], GrInfo, Nt, FollowI, FollowO) :-
    followProd(Prod, GrInfo, Nt, FollowI, FollowO1),
    followProds(ProdL, GrInfo, Nt, FollowO1, FollowO).


% followProd(+Production, +GrammarInfo, +Nt, +FollowIn, -FollowOut)
followProd([], _, _, Follow, Follow).

followProd([nt(Nt1) | Prod], GrInfo, Nt2, FollowI, FollowO) :-
    GrInfo = grInfo(_, EpsL, First),
    firstForList(Prod, EpsL, First, ListsFirst),
    member(follow(Nt1, Nt1Val), FollowI),
    addListsRemDups(Nt1Val, ListsFirst, Nt1Val1),
    (isProdEps(Prod, EpsL) ->
        member(follow(Nt2, Nt2Val), FollowI),
        addListsRemDups(Nt1Val1, Nt2Val, Nt1Val2)
    ;
        Nt1Val2 = Nt1Val1
    ),
    changeMapVal(follow(Nt1, Nt1Val2), follow, FollowI, FollowO1),
    followProd(Prod, GrInfo, Nt2, FollowO1, FollowO).

followProd([X | Prod], GrInfo, Nt, FollowI, FollowO) :-
    terminal(X),
    followProd(Prod, GrInfo, Nt, FollowI, FollowO).


% firstForList(+Production, +EpsilonNts, +First, -Lists'sFirst)
firstForList(Prod, EpsL, First, ListsFirst) :-
    firstForListWithDups(Prod, EpsL, First, ListsFirstWithDups),
    remove_dups(ListsFirstWithDups, ListsFirst).


% firstForListWithDups(+Production, +EpsilonNts, +First, -List'sFirst)
firstForListWithDups([], _, _, []).

firstForListWithDups([nt(Nt) | Prod], EpsL, First, ListsFirst) :-
    member(first(Nt, NtVal), First),
    append(NtVal, ListsFirst1, ListsFirst),
    (member(Nt, EpsL) ->
        firstForListWithDups(Prod, EpsL, First, ListsFirst1)
    ;
        ListsFirst1 = []
    ).

firstForListWithDups([X | _], _, _, [X]) :-
    terminal(X).


% select(+Grammar, -Select)
% Finding the select sets for all the productions.
% The order from the Grammar will be preserved.
select(Gr, Select) :-
    first(Gr, First),
    eps(Gr, EpsL),
    follow(Gr, Follow),
    selectNts(Gr, grInfo(Gr, First, EpsL, Follow), Select).


% selectNt(+Grammar, +GrammarInfo, -Select)
selectNts([], _, []).

selectNts([prod(Nt, ProdL) | Gr], GrInfo, [SelectNt | Select]) :-
    selectProds(ProdL, GrInfo, Nt, SelectNt),
    selectNts(Gr, GrInfo, Select).


% selectProds(+ProductionsList, +GrammarInfo, +Nt, -SelectNt)
selectProds([], _, _, []).

selectProds([Prod | ProdL], GrInfo, Nt, [SelectProd | SelectNt]) :-
    selectProd(Prod, GrInfo, Nt, SelectProd),
    selectProds(ProdL, GrInfo, Nt, SelectNt).


% selectProd(+Production, +GrammarInfo, +Nt, -SelectProd)
selectProd(Prod, grInfo(_, First, EpsL, Follow), Nt, SelectProd) :-
    firstForList(Prod, EpsL, First, ListsFirst),
    (isProdEps(Prod, EpsL) ->
        member(follow(Nt, NtVal), Follow),
        addListsRemDups(ListsFirst, NtVal, SelectProd)
    ;
        SelectProd = ListsFirst
    ).


% jestLL1(+Grammar)
% Checking whether grammar is LL(1).
jestLL1(Gr) :-
    \+ isLhsRecursion(Gr),
    select(Gr, Select),
    isLL1Nts(Select).


% isLL1Nts(+Select)
isLL1Nts([]).

isLL1Nts([SelectNt | Select]) :-
    isLL1Prods(SelectNt),
    isLL1Nts(Select).


% isLL1Prods(+SelectNt)
isLL1Prods([]).

isLL1Prods([SelectProd | SelectNt]) :-
    separableFromAll(SelectProd, SelectNt),
    isLL1Prods(SelectNt).


% separableFromAll(+L, +LL)
% Checking whether there is no element E such that E is in L, and E is in L1
% for some L1 in LL.
separableFromAll(_, []).

separableFromAll(L1, [L2 | LL]) :-
    separable(L1, L2),
    separableFromAll(L1, LL).


% separable(+L1, +L2)
separable(L1, L2) :- \+ intersect(L1, L2).


%intersect(+L1, +L2)
intersect(L1, L2) :-
    member(X, L1),
    member(X, L2).


% remDirectLeftRec(+GrammarIn, -GrammarOut)
% Removing direct left recursion.
remDirectLeftRec([], []).

remDirectLeftRec([prod(Nt, ProdL) | GrI], GrO) :-
    splitProductions(ProdL, Nt, N, M),
    M \= [],
    (N \= [] ->
        fixDirectLeftRec(Nt, N, M, X, Y),
        GrO = [X, Y | GrO1]
    ;
        GrO = [prod(Nt, M) | GrO1]
    ),
    remDirectLeftRec(GrI, GrO1).


% fixDirectLeftRec(+Nt, +N, +M, -P1, -P2)
% N == the list of tails for all productions for Nt that begin with Nt
%      (the head is Nt).
% M == all the productions for Nt that don't begin with Nt
% P1, P2 - a pair of new prod(...) such that the recursion is fixed
fixDirectLeftRec(Nt, N, M, prod(Nt, Y), prod(Nt1, [[] | X])) :-
    atom_concat(Nt, '1', Nt1),
    appendForAll(N, [nt(Nt1)], X),
    appendForAll(M, [nt(Nt1)], Y).


% appendforAll(+LL1, +L, -LL2)
appendForAll([], _, []).

appendForAll([L | LL], X, [L1 | LL1]) :-
    append(L, X, L1),
    appendForAll(LL, X, LL1).


% splitProductions(+ProductionsList, +Nt, -N, -M)
% N == the list of tails for all productions for Nt that begin with Nt
%      (the head is Nt).
% M == all the productions for Nt that don't begin with Nt
splitProductions([], _, [], []).

splitProductions([[nt(Nt) | Prod] | ProdL], Nt, N, M) :-
    (Prod = [] ->
        N = N1
    ;
        N = [Prod | N1]
    ),
    splitProductions(ProdL, Nt, N1, M).

splitProductions([[X | Prod] | ProdL], Nt, N, [[X | Prod] | M]) :-
    X \= nt(Nt),
    splitProductions(ProdL, Nt, N, M).

splitProductions([[] | ProdL], Nt, N, [[] | M]) :-
    splitProductions(ProdL, Nt, N, M).


% remLeftRec(+GrammarIn, -GrammarOut)
% Removing left recursion according to algorithm from:
% en.wikipedia.org/wiki/Left_recursion.
remLeftRec(GrI, GrO) :-
    \+ jestCykl(GrI),
    (isLhsRecursion(GrI) ->
        eps(GrI, EpsL),
        remEps(GrI, GrI1),
        GrI = [prod(St, _) | _],
        (GrI1 = [] ->
            GrO = [prod(St, [[]])]
        ;
            order(GrI1, 0, NtL, Ord),
            remLeftRec(NtL, GrI1, Ord, GrO1),
            GrO1 = [prod(St, StVal) | Rest],
            (isProdEps([nt(St)], EpsL) ->
                GrO = [prod(St, [[] | StVal]) | Rest]
            ;
                GrO = GrO1
            )
        )
    ;
        GrO = GrI
    ).


% order(+Grammar, +Cnt, -NtsList, -Order)
% Establishing some order for all nonterminals, starting with Cnt.
% NtsList == list of all nonterminals according to Order
% Order == for all nonterminals a symbol ord(Nt, NtOrd).
order([], _, [], []).

order([prod(Nt, _) | Gr], I, [Nt | NtL], [ord(Nt, I) | Ord]) :-
    I1 is I + 1,
    order(Gr, I1, NtL, Ord).


% remLeftRec(+NtsList, +GrammarIn, +Order, -GrammarOut)
remLeftRec([], Gr, _, Gr).

remLeftRec([Nt | NtL], GrI, Ord, GrO) :-
    member(prod(Nt, ProdL), GrI),
    member(ord(Nt, NtOrd), Ord),
    remLeftRecProdsIter(ProdL, NtOrd, GrI, Ord, ProdLO),
    remDirectLeftRec([prod(Nt, ProdLO)], GrTmp),
    updateGrammar(GrTmp, GrI, GrI1),
    remLeftRec(NtL, GrI1, Ord, GrO).


% remLeftRecProdsIter(+ProductionsListIn, +NtOrd, +Grammar, +Ord,
%                     -ProductionsListOut)
% Iterating for the given nonterminal as long as there is some nonterminal
% in the productions that has lower order.
remLeftRecProdsIter(ProdLI, NtOrd, Gr, Ord, ProdLO) :-
    remLeftRecProds(ProdLI, NtOrd, Gr, Ord, 0, ProdLO1, Cnt),
    (Cnt > 0 ->
        remLeftRecProdsIter(ProdLO1, NtOrd, Gr, Ord, ProdLO)
    ;
        ProdLO = ProdLO1
    ).


% remLeftRecProds(+ProductionsListIn, +NtOrd, +Grammar, +CntIn,
%                 -ProductionsListOut, -CntOut)
% Cnt is for counting the number of changes. However, we only care whether
% it's zero or not.
remLeftRecProds([], _, _, _, Cnt, [], Cnt).

remLeftRecProds([Prod | ProdLI], Nt1Ord, Gr, Ord, CntI, ProdLO, CntO) :-
    (Prod = [nt(Nt2) | B], member(ord(Nt2, Nt2Ord), Ord), Nt2Ord < Nt1Ord ->
        member(prod(Nt2, Nt2Val), Gr),
        appendForAll(Nt2Val, B, Nt1Val),
        append(Nt1Val, ProdLO1, ProdLO),
        CntO1 is CntI + 1
    ;
        ProdLO = [Prod | ProdLO1],
        CntO1 is CntI
    ),
    remLeftRecProds(ProdLI, Nt1Ord, Gr, Ord, CntO1, ProdLO1, CntO).

% remEps(+GrIn, -GrOut)
% Removing all epsilon productions.
% GrIn should not accept empty word.
% If it does, the resulting grammar will accept all the words the previous one
% accepts except an empty one.
remEps(GrIn, GrOut) :-
    eps(GrIn, EpsL),
    remEps(GrIn, EpsL, GrOut).

% remEps(+GrIn, +EpsilonNts, -GrOut)
% Removing all epsilon productions.
remEps(GrIn, EpsL, GrOut) :-
    remEpsHardIter(GrIn, GrOut1),
    remEpsNts(GrOut1, EpsL, GrOut).


% remEpsHardIter(+GrammarIn, -GrammarOut)
remEpsHardIter(GrIn, GrOut) :-
    remEpsHardNts(GrIn, Deleted, GrOut1),
    (Deleted = [] ->
        GrOut = GrOut1
    ;
        remDelNts(GrOut1, Deleted, GrOut2),
        remEpsHardIter(GrOut2, GrOut)
    ).


% remEpsHardNts(+GrammarIn, -Deleted, -GrammarOut)
remEpsHardNts([], [], []).

remEpsHardNts([prod(Nt, ProdL) | GrIn], Deleted, GrOut) :-
    delete(ProdL, [], ProdLOut),
    (ProdLOut = [] ->
        Deleted = [Nt | Deleted1],
        GrOut = GrOut1
    ;
        Deleted = Deleted1,
        GrOut = [prod(Nt, ProdLOut) | GrOut1]
    ),
    remEpsHardNts(GrIn, Deleted1, GrOut1).

% remDelNts(+GrammarIn, +Deleted, -GrammarOut)
% Removing deleted nonterminals from all productions (that is replacing them
% with an empty word).
remDelNts([], _, []).

remDelNts([prod(Nt, ProdLIn) | GrIn], Deleted, [prod(Nt, ProdLOut) | GrOut]) :-
    remDelProds(ProdLIn, Deleted, ProdLOut),
    remDelNts(GrIn, Deleted, GrOut).


% remDelProds(+ProductionsListIn, +Deleted, -ProductionsListOut)
remDelProds([], _, []).

remDelProds([ProdIn | ProdLIn], Deleted, [ProdOut | ProdLOut]) :-
    remDelProd(ProdIn, Deleted, ProdOut),
    remDelProds(ProdLIn, Deleted, ProdLOut).

% remDelProd(+ProductionIn, +Deleted, -ProductionOut)
remDelProd([], _, []).

remDelProd([X | ProdIn], Deleted, [X | ProdOut]) :-
    terminal(X),
    remDelProd(ProdIn, Deleted, ProdOut).

remDelProd([nt(Nt) | ProdIn], Deleted, ProdOut) :-
    (member(Nt, Deleted) ->
        ProdOut = ProdOut1
    ;
        ProdOut = [nt(Nt) | ProdOut1]
    ),
    remDelProd(ProdIn, Deleted, ProdOut1).


% remEpsNts(+GrammarIn, +EpsilonNts, -GrammarOut)
% Change the productions in grammar substituting all nonterminals in
% EpsL with either [] or themselves. However, we remove empty productions that
% will be created. The resulting grammar will not accept empty word.
remEpsNts([], _, []).

remEpsNts([prod(Nt, ProdLIn) | GrIn], EpsL, [prod(Nt, ProdLOut2) | GrOut]) :-
    remEpsProds(ProdLIn, EpsL, ProdLOut),
    delete(ProdLOut, [], ProdLOut1),
    % ProdLOut1 can't be empty here
    remove_dups(ProdLOut1, ProdLOut2),
    remEpsNts(GrIn, EpsL, GrOut).


% remEpsProds(+ProductionsListIn, +EpsilonNts, -ProductionsListOut)
remEpsProds([], _, []).

remEpsProds([Prod | ProdLIn], EpsL, ProdLOut) :-
    remEpsProd(Prod, [[]], EpsL, NewProds),
    append(NewProds, ProdLOut1, ProdLOut),
    remEpsProds(ProdLIn, EpsL, ProdLOut1).


% remEpsProd(+Productions, +Prefixes, +EpsilonNts, -NewProductions)
% Prefixes contains all possible productions' prefixes created.
remEpsProd([], NewProds, _, NewProds).

remEpsProd([nt(Nt) | Prod], Pref, EpsL, NewProds) :-
    appendForAll(Pref, [nt(Nt)], Pref1),
    (member(Nt, EpsL) ->
        append(Pref, Pref1, Pref2)
    ;
        Pref2 = Pref1
    ),
    remEpsProd(Prod, Pref2, EpsL, NewProds).

remEpsProd([X | Prod], Pref, EpsL, NewProds) :-
    terminal(X),
    appendForAll(Pref, [X], Pref1),
    remEpsProd(Prod, Pref1, EpsL, NewProds).


% updateGrammar(+Grammar1, +Grammar2In, -Grammar2Out)
% Updating grammar2 with grammar1. If there are nonterminals with both of
% them, we choose symbols from grammar1.
updateGrammar([], Gr, Gr).

updateGrammar([prod(Nt, NtVal) | Gr], GrI, GrO) :-
    (member(prod(Nt, _), GrI) ->
        changeMapVal(prod(Nt, NtVal), prod, GrI, GrO1)
    ;
        append(GrI, [prod(Nt, NtVal)], GrO1)
    ),
    updateGrammar(Gr, GrO1, GrO).


% terminal(+Symbol)
terminal(X) :- \+ nonterminal(X).


% nonterminal(+Symbol)
nonterminal(nt(_)).


% addListsRemDups(+L1, +L2, -L3)
addListsRemDups(A, B, X) :-
    append(A, B, Y),
    remove_dups(Y, X).


% We have a common datastructure for many predicates. We'll call it a Map.
% For a given Name, a Map is a list of Name(Key, Value), where Value is a
% list. Thanks to that we'll avoid redundancy of code.

% changeMapVal(+NewValue, +Name, +MapIn, -MapOut)
% NewValue == Name(Key, Value), where Value is a List
% We want to replace mapping in MapIn for Key with Value.
changeMapVal(New, Name, [E1 | MapI], [E2 | MapO]) :-
    New =..[Name, Nt, _],
    (E1 =..[Name, Nt, _] ->
        E2 = New,
        MapO = MapI
    ;
        E2 = E1,
        changeMapVal(New, Name, MapI, MapO)
    ).


% countAllVals(+Map, +Name, -Size)
% Size if the sume of sizes for all elements in Map.
countAllVals(Map, Name, Size) :- countAllVals(Map, Name, 0, Size).

countAllVals([], _, Size, Size).

countAllVals([E | Map], Name, SizeI, SizeO) :-
    E =..[Name, _, Val],
    length(Val, ValSize),
    SizeI1 is SizeI + ValSize,
    countAllVals(Map, Name, SizeI1, SizeO).


% initMap(+Grammar, +Name, -Map).
% Init map with empty values and keys for all nonterminals.
initMap([], _, []).

initMap([prod(Nt, _) | Gr], Name, [E | Map]) :-
    E =..[Name, Nt, []],
    initMap(Gr, Name, Map).
