:- [dl337614].
:- use_module(library(lists)).

grammar(ex1, [prod('E', [[nt('E'), '+', nt('T')],  [nt('T')]]),
              prod('T', [[id],  ['(', nt('E'), ')']])   ]).


grammar(ex2, [prod('A', [[nt('A'), x], [x]])]).    % nie LL(1), LeftRec

grammar(ex3, [prod('A', [[x, nt('A')], [x]])]).    % nie LL(1)

                                                   % LL(2)
grammar(ex4, [prod('A', [[a, nt('B')], [a, nt('C')]]),
              prod('B', [[b]]),
              prod('C', [[c]])   ]).

                                                   % LL(1)
grammar(ex5, [prod('A', [[a, nt('R')]]),
              prod('R', [[nt('B')], [nt('C')]]),
              prod('B', [[b]]),
              prod('C', [[c]])   ]).

                                                   % LL(1), nie jest SLR(1)
grammar(ex6, [prod('S', [[nt('A'), a, nt('A'), b],
                         [nt('B'), b, nt('B'), a]]),
              prod('A', [[]]),
              prod('B', [[]])    ]).

grammar(ex7, [prod('A', [[a], [nt('B'), x]]),
              prod('B', [[b], [nt('A'), y]]) ]).

grammar(ex8, [prod('A', [[nt('A'), a]]) ]).        % nie można usunąć LeftRec

grammar(ex9, [prod('S', [[nt('B')], [nt('A')]]),
              prod('A', [[nt('B')], [nt('C')]]),
              prod('B', [[b]]),
              prod('C', [[]])]).

grammar(ex10, [prod('A', [[nt('A')], [a]])]).

grammar(ex11, [prod('A', [[nt('B'), nt('A'), nt('C')]]),
               prod('B', [[nt('D')]]),
               prod('D', [[]]),
               prod('C', [[]]) ]).
grammar(ex12, [prod('A', [[nt('D'), nt('C'), nt('A')]]),
               prod('C', [[]]),
               prod('D', [[d]]) ]).

grammar(ex13, [prod('A', [[nt('B')], [a], [nt('C'), g]]),
               prod('B', [[nt('A')], [b]]),
               prod('C', [[nt('D'), nt('E'), nt('F')]]),
               prod('D', [[], [d]]),
               prod('E', [[], [e]]),
               prod('F', [[], [f]]) ]).

grammar(ex14, [prod('A', [[nt('B'), nt('C'), nt('D')], [nt('A'), a]]),
               prod('B', [[b], []]),
               prod('C', [[c], []]),
               prod('D', [[d], []])]).

grammar(ex15, [prod('A', [[a, nt('A')], []])]).

grammar(ex16, [prod('A', [[nt('B')], [nt('C')]]),
               prod('B', [[a]]),
               prod('C', [[a]])]).

grammar(ex17, [prod('A', [[], [nt('A')]])]).
grammar(ex18, [prod('A', [[nt('B')]]),
               prod('B', [[nt('A')], []])]).

grammar(ex19, [prod('A', [[nt('B')], [nt('C')]]),
               prod('B', [[]]),
               prod('C', [[]])]).

grammar(ex20, X) :- createPath(90, 'A', P, Tail),
                    append(P, [prod(Tail, [[nt('B')], [nt('C')]]),
                               prod('B', [[]]),
                               prod('C', [[]])], X).
grammar(ex21, X) :- createPath(90, 'A', P, Tail),
                    append(P, [prod(Tail, [[a]])], X).
grammar(ex22, [prod('A', [[nt('B')], [a]]),
               prod('B', [[nt('C')]]),
               prod('C', [[]])]).
grammar(ex23, [prod('A', [[nt('C'), nt('B'), nt('D')], []]),
               prod('B', [[nt('C'), nt('A'), nt('D')]]),
               prod('C', [[]]),
               prod('D', [[a]])]).
grammar(ex24, [prod('A', [[nt('A')]])]).
grammar(ex25, [prod('A', [[nt('B'), b]]),
               prod('B', [[nt('C'), c]]),
               prod('C', [[nt('A'), a]])]).

createPath(N, Base, P, Tail) :- createPath(0, N, Base, Base, P, Tail).

createPath(I1, I2, Base, Last, [prod(Last, [[nt(X)]]) | P], Tail) :-
    I1 < I2,
    I11 is I1 + 1,
    getNewName(I1, Base, X),
    createPath(I11, I2, Base, X, P, Tail).

createPath(I, I, _, Tail, [], Tail).

getNewName(0, Old, New) :- atom_concat(Old, '0', New).
getNewName(I, Old, New) :-
    I > 0,
    getNewName(I, List),
    append(List, [Old], List1),
    concatAtoms(List1, '', New).

getNewName(0, []).
getNewName(I, [Y | L]) :- I > 0, X is mod(I, 10), const(X, Y), I1 is I div 10, getNewName(I1, L).

concatAtoms([], A, A).
concatAtoms([X | L], A, New) :- atom_concat(X, A, A1), concatAtoms(L, A1, New).

const(0, '0').
const(1, '1').
const(2, '2').
const(3, '3').
const(4, '4').
const(5, '5').
const(6, '6').
const(7, '7').
const(8, '8').
const(9, '9').

test(1) :- grammar(ex4, G), analizaLL(G, [a, b], DW), DW = 'A'(a, 'B'(b)).
% whitebox test
test(2) :- grammar(ex9, G), eps(G, X), permutation(X, ['C', 'S', 'A']).

test(3) :- grammar(ex10, G), jestCykl(G).

test(4) :- grammar(ex8, G), \+ jestCykl(G).

test(5) :- grammar(ex11, G), jestCykl(G).

test(6) :- grammar(ex12, G), \+ jestCykl(G).
% whitebox test - assuming existence of first operator
test(7) :- grammar(ex1, G), first(G, X),
           allMapsSame(first, X, [first('E', [id, '(']), first('T', [id, '('])]).
% whitebox test - assuming existence of first operator
test(8) :- grammar(ex13, G), first(G, X),
           allMapsSame(first, X, [first('A', [d, e, f, g, a, b]),
                                  first('B', [d, e, f, g, a, b]),
                                  first('C', [d, e, f]),
                                  first('D', [d]),
                                  first('E', [e]),
                                  first('F', [f])]).

test(9) :- grammar(ex14, G), follow(G, X),
           allMapsSame(follow, X, [follow('A', [a, #]),
                                   follow('B', [a, c, d, #]),
                                   follow('C', [d, a, #]),
                                   follow('D', [a, #])]).

test(10) :- grammar(ex14, G), select(G, X), sameSelects(X, [[[a, b, c, d, #], [a, b, c, d]],
                                                            [[b], [c, d, a, #]],
                                                            [[c], [d, a, #]],
                                                            [[d], [a, #]]]).

test(11) :- grammar(ex1, G), \+ jestLL1(G).
test(12) :- grammar(ex2, G), \+ jestLL1(G).
test(13) :- grammar(ex3, G), \+ jestLL1(G).
test(14) :- grammar(ex4, G), \+ jestLL1(G).
test(15) :- grammar(ex5, G), jestLL1(G).
test(16) :- grammar(ex6, G), jestLL1(G).
test(17) :- grammar(ex15, G), jestLL1(G).
test(18) :- grammar(ex15, G), analizaLL(G, [a, a, a], _).
test(19) :-  P = [prod('T', [[id], ['(', nt('E'), ')']])], remDirectLeftRec(P, P).
% whitebox test
test(20) :- remDirectLeftRec([prod('E', [[nt('E'), '+', nt('T')], [nt('T')]])], LP),
            LP = [prod('E', [[nt('T'), nt('E1')]]),
                  prod('E1', [[], ['+', nt('T'), nt('E1')]])].
test(21) :- grammar(ex16, G), findall(D, analizaLL(G, [a], D), W),
            permutation(W, ['A'('B'(a)), 'A'('C'(a))]).
%whitebox test
test(22) :- grammar(ex17, G), remDirectLeftRec(G, G1), G1 = [prod('A', [[]])].
% whitebox test
test(23) :- grammar(ex7, G), remLeftRec(G, G1), G1 = [prod('A', [[a], [nt('B'), x]]),
                                                      prod('B', [[b, nt('B1')], [a, y, nt('B1')]]),
                                                      prod('B1', [[], [x,y,nt('B1')]]) ].
test(24) :- grammar(ex8, G), \+ remLeftRecSuccess(G).
test(25) :- grammar(ex1, G1), remLeftRec(G1, G), inGrammar(G, [id]),
                                                 inGrammar(G, ['(', id, ')']),
                                                 outOfGrammar(G, [id, '+', ident]),
                                                 inGrammar(G, [id, '+', id]).
% controversial test, there is a cycle, but recursion can be removed
% test(26) :- grammar(ex18, G1), remLeftRec(G1, G), inGrammar(G, []).
test(26) :- grammar(ex18, G), \+ remLeftRecSuccess(G).
test(27) :- grammar(ex19, G), \+ jestLL1(G).
% performance test
test(28) :- grammar(ex20, G), \+ jestLL1(G).
% performance test
test(29) :- grammar(ex21, G), jestLL1(G).
% whitebox test
test(30) :- grammar(ex22, G), remEps(G, G1), length(G1, 1), analizaLL(G1, [a], _).
test(31) :- grammar(ex23, G1), remLeftRec(G1, G), inGrammar(G, []),
                                                  inGrammar(G, [a, a]),
                                                  outOfGrammar(G, [a, a, a]),
                                                  outOfGrammar(G, [b]).
test(32) :- grammar(ex1, G), \+ jestLL1(G).
test(33) :- grammar(ex24, G), \+ jestLL1(G).
test(34) :- grammar(ex25, G), isLhsRecursion(G).
inGrammar(G, W) :- analizaLL(G, W, _).
outOfGrammar(G, W) :- \+ inGrammar(G, W).

remLeftRecSuccess(G) :- remLeftRec(G, _).

sameSelects([], []).
sameSelects([LN1 | L1], [LN2 | L2]) :-
    sameSelects1(LN1, LN2),
    sameSelects(L1, L2).

sameSelects1([], []).
sameSelects1([P1 | LP1], [P2 | LP2]) :-
    permutation(P1, P2),
    sameSelects1(LP1, LP2).

allMapsSame(_, [], []).
allMapsSame(Name, [M1 | L1], [M2 | L2]) :-
    M1 =..[Name, X, Val1],
    M2 =..[Name, X, Val2],
    permutation(Val1, Val2),
    allMapsSame(Name, L1, L2).

check(N) :-
    write('Test '), write(N),
    (test(N) ->
        write(' passed'),
        nl
    ;
        write(' failed'),
        nl,
        fail
    ).

runTest(I, N) :- I > N.
runTest(I, N) :- I =< N, check(I), I1 is I + 1, runTest(I1, N).

test :- runTest(1, 34).
